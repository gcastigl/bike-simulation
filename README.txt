Pasos para ejecutar simplemente correr la aplicacion:

1 - Extraer del archivo natives.rar los archivos de acuerdo al sistema operativo
2 - Configurar la carpeta data con todos los archivos necesarios para la simulacion
3 - Ejecutar el comando

	java -Djava.library.path={ruta/a/carpeta/con/nativos/seleccionados} -jar ecobici.jar {ruta/a/carpeta/data}
	
	A modo de ejemplo:
	
		java -Djava.library.path=./native -jar ecobici.jar ./data/


package ar.edu.itba.systemsim.gui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import ar.edu.itba.systemsim.ecobici.SimulationTime;

public class KeyboardHandler {

	private SimulationTime _time;

	public KeyboardHandler(SimulationTime time) {
		_time = time;
	}
	
	public void update(GameContainer gc, int elapsedTime) throws SlickException {
		Input input = gc.getInput();
		if (input.isKeyDown(Input.KEY_ADD) || input.isKeyPressed(Input.KEY_A)) {
			_time.incrementUpdateTime();
		}
		if (input.isKeyDown(Input.KEY_MINUS) || input.isKeyDown(Input.KEY_SUBTRACT) || input.isKeyPressed(Input.KEY_S)) {
			_time.decrementUpdateTime();
		}
		if (input.isKeyPressed(Input.KEY_P)) {
			gc.setPaused(!gc.isPaused());
		}
		if (input.isKeyPressed(Input.KEY_R)) {
			gc.reinit();
		}
		
	}

}

package ar.edu.itba.systemsim.gui;

import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import ar.edu.itba.systemsim.ecobici.BikeArea;
import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeTruck;
import ar.edu.itba.systemsim.util.StringUtil;

import com.google.common.base.Function;

public class StatsRenderer {
	
	private static int LINE_HEIGHT = 15;
	private GuiTable<BikeStation> _stationsTable;
	private GuiTable<BikeTruck> _trucksTable;

	public StatsRenderer() {
		_stationsTable = new GuiTable<>(740, LINE_HEIGHT * 9);
		String[] stationColumns = {"id", "Nombre", "#Bicis", "Cola", "Sin Bici"};
		_stationsTable.configure(stationColumns, new Function<BikeStation, String[]>() {
			@Override
			public String[] apply(BikeStation station) {
				String[] columns = new String[5];
				columns[0] = station.getId() + "";
				columns[1] = station.getName();
				columns[2] = station.getBikeCount() + "";
				columns[3] = station.getSinkSize() + "";
				columns[4] = station.getSink().getStats().getLeftOut() + "";
				return columns;
			}
		});
		_trucksTable = new GuiTable<>(740, 630);
		// Spaces are used only as padding for the text on the GUI!
		String[] truckColumns = {"Nombre", "Origen    ", "Destino   ", "#Bicis", "Time", "Viajes"};
		final PeriodFormatter formatter = new PeriodFormatterBuilder()
			.appendMinutes().appendSuffix(":").appendSeconds().appendSuffix(" [M]").toFormatter();
		_trucksTable.configure(truckColumns, new Function<BikeTruck, String[]>() {
			@Override
			public String[] apply(BikeTruck bikeTruck) {
				String[] columns = new String[6];
				columns[0] = bikeTruck.getId() + "";
				BikeStation origin = bikeTruck.getOrigin();
				columns[1] = (origin == null) ? "-" : StringUtil.trim(origin.getName(), 10);
				BikeStation destination = bikeTruck.getDestination();
				columns[2] = (destination == null) ? "-" : StringUtil.trim(destination.getName(),10);
				columns[3] = bikeTruck.getBikeCount() + "";
				Period period = new Period(Seconds.seconds((int) bikeTruck.getDuration())); 
				columns[4] = formatter.print(period.normalizedStandard());
				columns[5] = bikeTruck.getTravelCount() + "";
				return columns;
			}
		});
	}
	
	public void render(Graphics g, BikeArea bikeArea , GameContainer gc) {
		int i = 1;
		g.drawString("Estadisticas:", 730,  i++ * LINE_HEIGHT);
		g.drawString("Bicis en uso:" + bikeArea.bikesInUse() , 760,  i++ * LINE_HEIGHT);
		g.drawString("Bicis en estaciones:" + bikeArea.bikesInStations() , 760,  i++ * LINE_HEIGHT);
		g.drawString("Bicis en camiones:" + bikeArea.bikesOnTrucks() , 760,  i++ * LINE_HEIGHT);
		g.drawString("Viajes realizados:" + bikeArea.totalTrips() , 760,  i++ * LINE_HEIGHT);
		g.drawString("Tiempo ocioso (bicis):" + bikeArea.getBikeIdleTimePercentage() , 760,  i++ * LINE_HEIGHT);
		float notTransported = bikeArea.leftOutUsersPercetage();
		g.drawString("% No transportados:" + String.format("%.3f", notTransported) , 760,  i++ * LINE_HEIGHT);
		g.drawString("Estaciones:", 730,  i++ * LINE_HEIGHT);
		_stationsTable.render(g, bikeArea.getBikeStations());
		_trucksTable.render(g, bikeArea.getBikeTransporters());
	}
}

package ar.edu.itba.systemsim.gui;

import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;

public class KeyboardListener implements KeyListener {

	private BikeAreaRenderer _bikeAreaRenderer;
	
	public KeyboardListener(BikeAreaRenderer bikeAreaRenderer) {
		_bikeAreaRenderer = bikeAreaRenderer;
	}
	
	@Override
	public void inputEnded() {
		
	}

	@Override
	public void inputStarted() {
		
	}

	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	@Override
	public void setInput(Input arg0) {
		
	}

	@Override
	public void keyPressed(int arg0, char arg1) {
	}

	@Override
	public void keyReleased(int arg0, char arg1) {
		if (Character.isDigit(arg1)) {
			int number = Character.getNumericValue(arg1);
			number = number == 0 ? -1 : number;
			_bikeAreaRenderer.setTruckIdToDrawPath(number);
		}
	}

}

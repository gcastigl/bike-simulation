package ar.edu.itba.systemsim.gui;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class GuiTable<T> {

	private static final int GAP = 4;

	private int _startX, _startY;
	private String[] _columnNames;
	private Function<T, String[]> _columnBuilder;

	public GuiTable(int startX, int startY) {
		_startX = startX;
		_startY = startY;
	}
	
	public void configure(String[] columnNames, Function<T, String[]> columnBuilder) {
		_columnNames = columnNames;
		_columnBuilder = columnBuilder;
	}

	public void render(Graphics g, Collection<T> rows) {
		Map<String, Integer> columnsWidth = Maps.newHashMap();
		List<String[]> convertedRows = Lists.newLinkedList();
		updateColumnsWith(g, _columnNames, columnsWidth);
		convertedRows.add(_columnNames);
		for (T row : rows) {
			String[] columns = _columnBuilder.apply(row);
			updateColumnsWith(g, columns, columnsWidth);
			convertedRows.add(columns);
		}
		int hight = g.getFont().getHeight("A");
		int rowNumber = 1;
		int y = _startY;
		Color current = g.getColor();
		for (String[] columns : convertedRows) {
			g.setColor(current);
			int x = _startX;
			int columnIndex = 0;
			for (String colum : columns) {
				g.drawString(colum, x, y);
				x += columnsWidth.get(_columnNames[columnIndex]) + GAP;
				columnIndex++;
			}
			y += hight;
			rowNumber++;
			if (rowNumber % 6 == 0) {
				y += 2;
				g.setColor(Color.white);
				g.drawLine(_startX, y, x, y);
			}
		}
	}

	private void updateColumnsWith(Graphics g, String[] columns, Map<String, Integer> columnsWidth) {
		int columnIndex = 0;
		for (String column : columns) {
			String columnName = _columnNames[columnIndex];
			Integer width = columnsWidth.get(columnName);
			width = (width == null) ? 0 : width;
			columnsWidth.put(columnName, Math.max(width, getRenderWidth(g, column) + GAP));
			columnIndex++;
		}
	}
	
	private int getRenderWidth(Graphics g, String s) {
		return g.getFont().getWidth(s);
	}
	
}

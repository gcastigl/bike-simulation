package ar.edu.itba.systemsim.gui;

import java.util.Collection;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.ecobici.BikeArea;
import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeTruck;
import ar.edu.itba.systemsim.ecobici.SimulationRunsStats;
import ar.edu.itba.systemsim.ecobici.SimulationTime;

public class BikeAreaRenderer {

	private static final int MAP_WIDTH = 700;
	private static final int MAP_HIGHT = 700;
	
	private final SimulationRunsStats simResults = SimulationRunsStats.instance();
	private final EventDispatcher dispatcher = EventDispatcher.instance();

	private int truckIdToDrawPath;
	
	private Line connectingLine = new Line(0, 0);
	private Image backgroundImg;
	private Circle bikeStaionShape = new Circle(0, 0, 5);
	private Circle truckShape = new Circle(0, 0, 5);

	public BikeAreaRenderer(String backgroundImgPath) {
		try {
			backgroundImg = new Image(backgroundImgPath);
		} catch (SlickException e) {
			throw new IllegalStateException(e);
		}
	}

	public void render(Graphics g, BikeArea bikeArea, GameContainer gc) {
		backgroundImg.draw(3, 5, MAP_WIDTH, MAP_HIGHT);
		List<BikeStation> stations = bikeArea.getBikeStations();
		for (BikeStation currentStation : stations) {
			Vector2f stationLocation = currentStation.getLocation();
			g.setColor(getStationColor(currentStation));
			Vector2f location = currentStation.getLocation();
			bikeStaionShape.setCenterX(location.x);
			bikeStaionShape.setCenterY(location.y);
			g.fill(bikeStaionShape);
			g.setColor(Color.red);
			String stationName = currentStation.getId() + "(" + currentStation.getBikeCount() + ")"; 
			int width = g.getFont().getWidth(stationName);
			g.drawString(stationName, stationLocation.x - width / 2, stationLocation.y - 20);
		}
		drawTrucks(g, bikeArea.getBikeTransporters());
		g.setColor(Color.yellow);
		SimulationTime time = bikeArea.getTime();
		String bottomInfo = "Run #" + (simResults.getRunsCount() + 1) + " | ";
		bottomInfo += time.getCurrentTime().toString("HH:mm:ss") + " < " + time.getUpdateTime();
		bottomInfo += " | Event queue: " + dispatcher.queueSize();
		bottomInfo += " | Total Events: " + dispatcher.getTotalEvents();
		g.drawString(bottomInfo, 0, gc.getHeight() - 20);
	}
	
	private Color getStationColor(BikeStation station) {
		if (station.hasBikeShortage()) {
			return Color.red;
		} else if (station.hasExtraBikes()) {
			return Color.yellow;
		} else {
			return Color.green;
		}
	}
	
	private void drawTrucks(Graphics g, Collection<BikeTruck> bikeTrucks) {
		for (BikeTruck truck : bikeTrucks) {
			if (truck.getId() == truckIdToDrawPath) {
				g.setColor(Color.gray);
				List<BikeStation> path = truck.getPath().getPathAsList();
				for (int index = 0 ; index < path.size(); index++) {
					BikeStation from = path.get(index);
					BikeStation to = path.get((index + 1) % path.size());
					drawConnectingLine(g, from, to);
				}
			}
			g.setColor(Color.white);
			Vector2f location;
			if (truck.getOrigin() != null && truck.getDestination() != null && truck.getOrigin().distanceTo(truck.getDestination()) > 0.1f) {
				drawConnectingLine(g, truck.getOrigin(), truck.getDestination());
				location = getPoint(connectingLine, truck.completedPercentage());
			} else {
				location = truck.getOrigin().getLocation();
			}
			g.setColor((truck.getBikeCount() == 0) ? Color.pink : Color.blue);
			truckShape.setCenterX(location.x);
			truckShape.setCenterY(location.y);
			g.fill(truckShape);
			g.setColor(Color.blue);
			g.drawString(truck.getId() + " | " + truck.getBikeCount(), location.x, location.y);
		}
	}

	private void drawConnectingLine(Graphics g, BikeStation stFrom, BikeStation stTo) {
		connectingLine.set(stFrom.getLocation(), stTo.getLocation());
		g.draw(connectingLine);
	}

	private Vector2f getPoint(Line line, float percentage) {
		if (line.length() > 0.01) {
			Vector2f dir = new Vector2f(line.getDX(), line.getDY()).normalise();
			float len = line.length();
			Vector2f from = line.getStart();
			float x = from.x + dir.x * len * percentage;
			float y = from.y + dir.y  * len * percentage;
			return new Vector2f(x, y);
		}
		return line.getEnd();
	}

	public void setTruckIdToDrawPath(int truckIdToDrawPath) {
		this.truckIdToDrawPath = truckIdToDrawPath;
	}
	
}

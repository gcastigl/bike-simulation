package ar.edu.itba.systemsim.ecobici;

import java.util.List;

import org.apache.log4j.Logger;

public class UserTravelStrategy {

	private static final Logger logger = Logger.getLogger(UserTravelStrategy.class);
	
	private static final SimulationTime _time = SimulationTime.instance();
	private List<StationDestinationMatrix> _destinationMatrixList;
	private UserTimeTravel _userTimeTravel;
	
	public UserTravelStrategy(List<StationDestinationMatrix> destinationMatrixList, UserTimeTravel userTimeTravel) {
		_destinationMatrixList = destinationMatrixList;
		_userTimeTravel = userTimeTravel;
	}
	
	public BikeStation getDestination(BikeStation origin) {
		float random = (float) Math.random();
		int hour = _time.getCurrentTime().getHourOfDay();
		StationDestinationMatrix matrix = null;
		for (StationDestinationMatrix destinationMatrix : _destinationMatrixList) {
			if (destinationMatrix.timeIntervalContains(hour)) {
				matrix = destinationMatrix;
			}
		}
		if (matrix == null) {
			logger.error("Horario " + hour + " NO tiene una matriz de destino. Usando default");
			matrix = _destinationMatrixList.get(0);
		}
		return matrix.getDestination(origin, random);
	}
	
	public long getTravelTime(BikeStation origin, BikeStation destination) {
		return (long) _userTimeTravel.getTravelTime(origin, destination);
	}
}

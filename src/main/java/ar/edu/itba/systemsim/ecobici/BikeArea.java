package ar.edu.itba.systemsim.ecobici;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.Seconds;

import ar.edu.itba.systemsim.core.Producer;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class BikeArea {

	private static final SimulationTime _time = SimulationTime.instance();

	private List<Producer<User>> _userProducers;
	private List<BikeStation> _bikeStations;
	private List<BikeUser> _bikeUsers;
	private List<BikeTruck> _bikeTrucks;
	private Set<Bike> _allExistingBikes;
	
	public BikeArea() {
		_userProducers = Lists.newArrayList();
		_bikeStations = Lists.newLinkedList();
		_bikeUsers = Lists.newLinkedList();
		_bikeTrucks = Lists.newLinkedList();
		_allExistingBikes = Sets.newHashSet();
	}

	public void initialize() {
		for (Producer<User> producer : _userProducers) {
			producer.start();
		}
		for (BikeTruck bikeTruck : _bikeTrucks) {
			bikeTruck.start();
		}
	}
	
	public boolean isEndOfDay() {
		return _time.getCurrentTime().getHourOfDay() >= 20; 
	}

	public void addBikeStations(Collection<BikeStation> stations) {
		_bikeStations.addAll(stations);
		Collections.sort(_bikeStations, BikeStations.sortById());
		for (BikeStation station : stations) {
			_allExistingBikes.addAll(station.getBikes());
		}
	}

	public void add(BikeUser bikeUser) {
		_bikeUsers.add(bikeUser);
	}

	public void remove(BikeUser bikeUser) {
		_bikeUsers.remove(bikeUser);
	}
	
	public void addBikeTrucks(Collection<BikeTruck> biketrucks) {
		_bikeTrucks.addAll(biketrucks);
	}
	
	public void addProducer(Producer<User> producer) {
		_userProducers.add(producer);
	}

	public Collection<Producer<User>> getProducers() {
		return Collections.unmodifiableCollection(_userProducers);
	}

	public int countUsersBetween(BikeStation s1, BikeStation s2) {
		int count = 0;
		for (BikeUser bikeUser : _bikeUsers) {
			if (bikeUser.between(s1, s2)) {
				count++;
			}
		}
		return count;
	}

	public List<BikeStation> getBikeStations() {
		return _bikeStations;
	}
	
	public List<BikeTruck> getBikeTransporters() {
		return _bikeTrucks;
	}
	
	public SimulationTime getTime() {
		return _time;
	}
	
	public int bikesInUse() {
		return _bikeUsers.size();
	}
	
	public Collection<BikeUser> getBikeUsers() {
		return Collections.unmodifiableCollection(_bikeUsers);
	}
	
	public int bikesInStations() {
		int count = 0;
		for (BikeStation station : getBikeStations()) {
			count += station.getBikeCount();
		}
		return count;
	}

	public int bikesOnTrucks() {
		int count = 0;
		for (BikeTruck bikeTruck : getBikeTransporters()) {
			count += bikeTruck.getBikeCount();
		}
		return count;
	}
	
	private int usersLeftOut() {
		int count = 0;
		for (BikeStation station : getBikeStations()) {
			count += station.getSink().getStats()._leftOut;
		}
		return count;
	}
	
	private int usersServed() {
		int count = 0;
		for (BikeStation station : getBikeStations()) {
			count += station.getSink().getStats()._consumed;
		}
		return count;
	}
	
	public int totalTrips() {
		int count = 0;
		for (BikeStation station : getBikeStations()) {
			count += station.getReceivedCount();
		}
		return count;
	}
	
	public float getBikeIdleTimePercentage() {
		Seconds totalIdleTime = Seconds.seconds(0);
		for (Bike bike : _allExistingBikes) {
			totalIdleTime = totalIdleTime.plus(bike.getTotalIdleSeconds());
		}
		int totalSeconds = totalIdleTime.toPeriod().getSeconds();
		int simulationSeconds = _time.getTotalSimulationTime().toPeriod().getSeconds();
		return totalSeconds * 100.0f / (simulationSeconds * _allExistingBikes.size());
	}
	
	public float leftOutUsersPercetage() {
		int usersLeftOut = usersLeftOut();
		int totalUsersConsumed = usersLeftOut + usersServed();
		return (totalUsersConsumed == 0) ? 0.0f : (usersLeftOut * 100.0f / totalUsersConsumed);
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE).toString();
	}
}

package ar.edu.itba.systemsim.ecobici;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import ar.edu.itba.systemsim.core.Math;

import com.google.common.collect.Lists;

public class SimulationRunsStats {

	private static final SimulationRunsStats instance = new SimulationRunsStats();

	public static SimulationRunsStats instance() {
		return instance;
	}

	private List<RunStats> results = Lists.newLinkedList();

	private SimulationRunsStats() {
	}

	public void simulationFinished(BikeArea bikeArea) {
		RunStats runStats = new RunStats();
		runStats.run = results.size() + 1;
		runStats.leftOutUsersPercetage = bikeArea.leftOutUsersPercetage();
		runStats.bikeIdleTimePercentage = bikeArea.getBikeIdleTimePercentage();
		results.add(runStats);
	}

	public int getRunsCount() {
		return results.size();
	}

	public Collection<RunStats> getResults() {
		Collection<Float> leftOutUsersPercetages = new LinkedList<>();
		Collection<Float> bikeIdleTimePercentages = new LinkedList<>();
		for (RunStats runStat : results) {
			leftOutUsersPercetages.add(runStat.leftOutUsersPercetage);
			bikeIdleTimePercentages.add(runStat.bikeIdleTimePercentage);
		}
		System.out.println("leftOutUsersPercetages = " + Math.average().apply(leftOutUsersPercetages));
		System.out.println("Dev = " + new StandardDeviation().evaluate(Math.toDoubleArray().apply(leftOutUsersPercetages)));
		System.out.println(leftOutUsersPercetages);
		System.out.println("bikeIdleTimePercentages = " + Math.average().apply(bikeIdleTimePercentages));
		System.out.println("Dev = " + new StandardDeviation().evaluate(Math.toDoubleArray().apply(bikeIdleTimePercentages)));
		System.out.println(bikeIdleTimePercentages);
		return results;
	}

	private static final class RunStats {
		int run;
		float leftOutUsersPercetage;
		float bikeIdleTimePercentage;
		
		@Override
		public String toString() {
			return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
				.append("Corrida " + run)
				.append("No transportados", leftOutUsersPercetage)
				.append("Bike idle time", bikeIdleTimePercentage).build();
		}
	}
	
}

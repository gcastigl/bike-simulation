package ar.edu.itba.systemsim.ecobici;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.Iterables;

public class StationDestinationMatrix {
	
	private static final Logger logger = Logger.getLogger(StationDestinationMatrix.class);
	
	private int _startHour, _endHour;
	private Map<BikeStation, List<DestinationProbability>> destinationProbability;
	
	public StationDestinationMatrix(int startHour, int endHour) {
		destinationProbability = new HashMap<>();
		_startHour = startHour;
		_endHour = endHour;
	}

	public boolean timeIntervalContains(int hour) {
		return _startHour <= hour && hour < _endHour;
	}

	public void add(BikeStation from, BikeStation to, float p) {
		List<DestinationProbability> values = destinationProbability.get(from);
		if (values == null) {
			destinationProbability.put(from, values = new LinkedList<>());
		}
		float acumulatedProb = p; 
		if (!values.isEmpty()) {
			acumulatedProb += Iterables.getLast(values).acumulatedProb;
		}
		if (acumulatedProb > 1.1) {
			logger.error("Suma de probabilidades es > 1 => " + from.getId() + ", " + to.getId());
		}
		values.add(new DestinationProbability(to, acumulatedProb));
	}

	public BikeStation getDestination(BikeStation from, float random) {
		if (random > 1) {
			logger.error("Probabilidad superior a 1!! => " + random);
		}
		for (DestinationProbability destinationProb : destinationProbability.get(from)) {
			if (destinationProb.acumulatedProb > random) {
				return destinationProb.to;
			}
		}
		logger.error("Could not resolve target for: " + from + " | returning default");
		return destinationProbability.get(from).get(0).to;
	}

	@Override
	public String toString() {
		String s = "";
		for (BikeStation from : destinationProbability.keySet()) {
			s += from.getName() + "[" + destinationProbability.get(from) + "]\n";
		}
		return s;
	}
	
	private static class DestinationProbability {
		BikeStation to;
		float acumulatedProb;

		public DestinationProbability(BikeStation to, float acumulatedProb) {
			this.to = to;
			this.acumulatedProb = acumulatedProb;
		}
		
		@Override
		public String toString() {
			return String.format("%.3f", acumulatedProb);
		}
	}

}

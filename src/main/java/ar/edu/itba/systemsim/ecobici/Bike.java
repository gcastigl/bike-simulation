package ar.edu.itba.systemsim.ecobici;

import org.joda.time.LocalTime;
import org.joda.time.Seconds;

public class Bike {

	private static final SimulationTime time = SimulationTime.instance(); 

	private int _id;
	private LocalTime _lastReturnTime;
	private Seconds _totalIdleSeconds;

	public Bike(int id) {
		this._id = id;
		_totalIdleSeconds = Seconds.seconds(0);
	}

	public int getId() {
		return _id;
	}

	@Override
	public String toString() {
		return "Bike: " + _id;
	}
	
	public void retrievedFromStation() {
		Seconds idleSeconds = Seconds.secondsBetween(_lastReturnTime, time.getCurrentTime());
		_totalIdleSeconds = _totalIdleSeconds.plus(idleSeconds);
		_lastReturnTime = null;
	}

	public void returnedToStation() {
		_lastReturnTime = time.getCurrentTime();
	}
	
	public Seconds getTotalIdleSeconds() {
		Seconds idleSeconds = Seconds.seconds(0);
		if (_lastReturnTime != null) {
			idleSeconds = Seconds.secondsBetween(_lastReturnTime, time.getCurrentTime());	
		}
		return _totalIdleSeconds.plus(idleSeconds);
	}

}

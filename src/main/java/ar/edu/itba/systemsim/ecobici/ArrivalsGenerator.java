package ar.edu.itba.systemsim.ecobici;

import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.LocalTime;

import ar.edu.itba.systemsim.core.rand.ExponentialRandomDistribution;
import ar.edu.itba.systemsim.core.rand.RandomGenerator;

import com.google.common.collect.Lists;

public class ArrivalsGenerator {
	
	private static Logger logger = Logger.getLogger(ArrivalsGenerator.class);
	private static final SimulationTime time = SimulationTime.instance();
	
	private List<SpecificArrivalRate> arrivalsRates;
	
	public ArrivalsGenerator() {
		arrivalsRates = Lists.newLinkedList();
	}
	
	
	public void addArrivalRate(float mean, int startTime, int endTime) {
		if(!existsArrivalRate(startTime, endTime)){
			arrivalsRates.add(new SpecificArrivalRate(mean, startTime, endTime));
		}
	}
	
	private boolean existsArrivalRate(int startHour, int endHour) {
		LocalTime startTime = new LocalTime(startHour);
		LocalTime endTime = new LocalTime(endHour);
		for (SpecificArrivalRate ar : arrivalsRates) {
			if (ar._startTime == startTime && ar._endTime == endTime) {
				return true;
			}
		}
		return false;
	}
	
	public long getArrivalRate() {
		LocalTime actualTime = time.getCurrentTime();
		for (SpecificArrivalRate specificRate : arrivalsRates) {
			boolean isBeforeOrEquals = specificRate._startTime.isBefore(actualTime);
			isBeforeOrEquals |= specificRate._startTime.isEqual(actualTime);
			if (isBeforeOrEquals && specificRate._endTime.isAfter(actualTime)) {
				return (long) (specificRate.randomGenerator.generate()); //Random generated in seconds.
			}
		}
		logger.error("Unknown time interval " + actualTime);
		return (long) (arrivalsRates.get(0).randomGenerator.generate());
	}

	@Override
	public String toString() {
		String out = "";
		for (SpecificArrivalRate rate : arrivalsRates) {
			out += rate._startTime + " - " + rate._endTime + " - " + rate._mean + "\n";
		}
		return out;
	}
	
	private static class SpecificArrivalRate {
		LocalTime _startTime, _endTime;
		float _mean;
		RandomGenerator randomGenerator;
		
		public SpecificArrivalRate(float mean, int startTime, int endTime) {
			_startTime = new LocalTime(startTime, 0);
			_endTime = new LocalTime(endTime, 0);
			_mean = mean;
			randomGenerator = new ExponentialRandomDistribution(_mean);
		}
	}
	
}

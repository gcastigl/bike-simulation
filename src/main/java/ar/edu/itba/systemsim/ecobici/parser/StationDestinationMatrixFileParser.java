package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeStations;
import ar.edu.itba.systemsim.ecobici.StationDestinationMatrix;
import ar.edu.itba.systemsim.util.CollectionUtil;

public class StationDestinationMatrixFileParser {

	public List<StationDestinationMatrix> load(ConfigurationFiles configurations, Collection<BikeStation> stationList) throws IOException {
		Map<Integer, BikeStation> stations = CollectionUtil.asMap(stationList, BikeStations.getId());
		List<StationDestinationMatrix> destinationMatrixList = new LinkedList<StationDestinationMatrix>();
		for (File file :  configurations.getBikeUserDestinations()) {
			if (file.isFile()) {
				destinationMatrixList.add(createMatrix(file, stations));
			}
		}
		return destinationMatrixList;
	}

	private StationDestinationMatrix createMatrix(File file, Map<Integer, BikeStation> stations){ 
		Scanner scanner;
		int startHour = getStartHour(file.getName());
		int endHour = getEndHour(file.getName());
		StationDestinationMatrix destinationMatrix = new StationDestinationMatrix(startHour, endHour);
		try {
			scanner = new Scanner(file);
			String[] destinationIds = scanner.nextLine().split(" ");
			while (scanner.hasNextLine()) {
				String[] line = scanner.nextLine().split(" ");
				BikeStation from = stations.get(Integer.valueOf(line[0]));
				for (int i = 1; i < line.length; i++) {
					BikeStation to = stations.get(Integer.valueOf(destinationIds[i]));
					float p = Float.parseFloat(line[i]) / 100;
					destinationMatrix.add(from, to, p);
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return destinationMatrix;
	}
	
	private int getStartHour(String fileName) {
		return Integer.valueOf(fileName.split("a")[0]);
	}
	
	private int getEndHour(String fileName) {
		return Integer.valueOf(fileName.split("a")[1].split("\\.")[0]);
	}
}

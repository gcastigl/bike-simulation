package ar.edu.itba.systemsim.ecobici;

import ar.edu.itba.systemsim.core.Producer;
import ar.edu.itba.systemsim.core.rand.UniformRandomGenerator;

public class UserProducer extends Producer<User> {

	private BikeArea bikeArea;
	private BikeStation origin;
	private UserTravelStrategy userTravelStrategy;

	public UserProducer(UserTravelStrategy userTravelStrategy, BikeArea bikeArea, BikeStation origin, BikeStationUserArrival userArrival) {
		// TODO Agregar generador especifico para cada estacion/horario.
		super(origin.getSink(), userArrival, new UniformRandomGenerator(2, 20));
		this.bikeArea = bikeArea;
		this.origin = origin;
		this.userTravelStrategy = userTravelStrategy;
	}

	@Override
	protected User produce() {
		return new User(userTravelStrategy, bikeArea, origin);
	}

}

package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeStations;
import ar.edu.itba.systemsim.ecobici.UserTimeTravel;
import ar.edu.itba.systemsim.util.CollectionUtil;

public class UserTimeTravelParser {

	public UserTimeTravel load(ConfigurationFiles configurations, Collection<BikeStation> stationList) throws IOException {
		Map<Integer, BikeStation> stations = CollectionUtil.asMap(stationList, BikeStations.getId());
		UserTimeTravel timeTravel = new UserTimeTravel();
		for (File file : configurations.getBikeUserUseTimes()) {
			Properties properties = new Properties();
			properties.load(new FileInputStream(file));
			String[] stationIds = file.getName().replace(".txt", "").split("a");
			BikeStation from = stations.get(Integer.parseInt(stationIds[0]));
			BikeStation to = stations.get(Integer.parseInt(stationIds[1]));
			for (Entry<Object, Object> entries : properties.entrySet()) {
				String[] timeInterval = entries.getKey().toString().trim().split("-");
				int min = Integer.valueOf(timeInterval[0]);
				int max = Integer.valueOf(timeInterval[1]);
				float probability = Float.valueOf(entries.getValue().toString());
				timeTravel.add(from, to, min, max, probability);
			}
		}
		return timeTravel;
	}

}

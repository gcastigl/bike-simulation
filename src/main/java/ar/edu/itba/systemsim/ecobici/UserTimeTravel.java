package ar.edu.itba.systemsim.ecobici;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.rand.RandomGenerator;
import ar.edu.itba.systemsim.core.rand.UniformRandomGenerator;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class UserTimeTravel {
	private static Logger logger = Logger.getLogger(UserTimeTravel.class);
	private Map<BikeStation, Map<BikeStation, TravelTime>> timings;

	public UserTimeTravel() {
		timings = Maps.newHashMap();
	}
	
	public void add(BikeStation from, BikeStation to, int minTime, int maxTime, float p) {
		Map<BikeStation, TravelTime> mapByOrigin = timings.get(from);
		if (mapByOrigin == null) {
			timings.put(from, mapByOrigin = Maps.newHashMap());
		}
		TravelTime travelTime = mapByOrigin.get(to);
		if (travelTime == null) {
			mapByOrigin.put(to, travelTime = new TravelTime());
		}
		float cummulativeProb = p;
		if (!travelTime.timeTravelByProbability.isEmpty()) {
			cummulativeProb += Iterables.getLast(travelTime.timeTravelByProbability).commulativeProbability;
		}
		travelTime.timeTravelByProbability.add(new SpecificTimeTravel(cummulativeProb, minTime, maxTime));
	}
	
	public long getTravelTime(BikeStation origin, BikeStation destination) {
		TravelTime timeTravel = timings.get(origin).get(destination);
		float p = (float) Math.random();
		for (SpecificTimeTravel specificTime : timeTravel.timeTravelByProbability) {
			if (p < specificTime.commulativeProbability) {
				return (long) (specificTime.randomGenerator.generate() * 60); //Random generated in minutes, converted to seconds.
			}
		}
		logger.error("Unknown travel " + origin.getName() + origin.getId() + " - " + destination.getName() + destination.getId());
		return (long)(timeTravel.timeTravelByProbability.get(0).randomGenerator.generate() * 60);
	}
	
	private static final class TravelTime {
		List<SpecificTimeTravel> timeTravelByProbability = Lists.newLinkedList();
	}
	
	private static class SpecificTimeTravel {
		float commulativeProbability;
		RandomGenerator randomGenerator;
		
		public SpecificTimeTravel(float commulativeProbability, int minTime, int maxTime) {
			this.commulativeProbability = commulativeProbability;
			randomGenerator = new UniformRandomGenerator(minTime, maxTime);
		}
	}
}

package ar.edu.itba.systemsim.ecobici;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;
import ar.edu.itba.systemsim.core.rand.CappedGaussianRandomGenerator;
import ar.edu.itba.systemsim.util.CollectionUtil;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;

public class BikeTransporterPath {

	private CappedGaussianRandomGenerator _travelTimeGenerator = new CappedGaussianRandomGenerator(0, 0, 0, 0);
	private List<BikeStation> _pathList;
	private Map<BikeStation, BikeStation> _path = Maps.newHashMap();
	
	public BikeTransporterPath(List<BikeStation> path) {
		Assert.assertTrue(!CollectionUtil.hasNulls(path));
		_pathList = Collections.unmodifiableList(path);
		for (int i = 0; i < path.size(); i++) {
			int next = (i + 1) % path.size();
			_path.put(path.get(i), path.get(next));
		}
	}
	
	public long travelTime(BikeStation from, BikeStation to, float speed) {
		float meanTime = from.distanceTo(to) / speed;
		_travelTimeGenerator.setMean(meanTime);
		_travelTimeGenerator.setStDev(5);
		_travelTimeGenerator.setLimits(meanTime * .5f, meanTime * 1.5f);
		return (long) (_travelTimeGenerator.generate() * 60);
	}
	
	public BikeStation nextForFetchBikes(BikeStation current) {
		return findFirstInPath(current, BikeStations.withExtraBikes());
	}
	
	public BikeStation nextForDropBikes(BikeStation current) {
		return findFirstInPath(current, BikeStations.withBikeShortage());
	}

	private BikeStation findFirstInPath(BikeStation current, Predicate<BikeStation> filter) {
		boolean validates = false;
		if (!_path.containsKey(current)) {
			// the truck might not always be on its regular path, resume from beginning
			current = _pathList.get(0);
		}
		BikeStation next = current;
		do {
			next = _path.get(next);
			validates = filter.apply(next);
		} while (!validates && !next.equals(current));
		return validates ? next : null;
	}
	
	public List<BikeStation> getPathAsList() {
		return _pathList;
	}
}

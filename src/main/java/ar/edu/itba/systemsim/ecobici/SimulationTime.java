package ar.edu.itba.systemsim.ecobici;

import org.joda.time.LocalTime;
import org.joda.time.Seconds;

import ar.edu.itba.systemsim.core.Updatable;

public class SimulationTime implements Updatable {

	private static final SimulationTime instance = new SimulationTime();
	
	public static SimulationTime instance() {
		return instance;
	}
	
	private LocalTime currentTime;
	private Seconds totalSimulationTime;
	private int _secondsPerUpdate;

	private SimulationTime() {
		totalSimulationTime = Seconds.seconds(0);
	}
	
	@Override
	public void update() {
		currentTime = currentTime.plusSeconds(getUpdateTime());
		totalSimulationTime = totalSimulationTime.plus(getUpdateTime());
	}
	
	public void setTime(int hourOfDay, int minuteOfHour, int secondOfMinute) {
		currentTime = new LocalTime(hourOfDay, minuteOfHour, secondOfMinute);
		totalSimulationTime = Seconds.seconds(0);
	}

	public void setSecondsPerUpdate(int secondsPerUpdate) {
		_secondsPerUpdate = secondsPerUpdate;		
	}
	
	public LocalTime getCurrentTime() {
		return currentTime;
	}

	public boolean isMorning() {
		return getCurrentTime().getHourOfDay() < 12;
	}

	public int getUpdateTime() {
		return _secondsPerUpdate;
	}
	
	public void incrementUpdateTime() {
		_secondsPerUpdate++;
	}
	
	public void decrementUpdateTime() {
		_secondsPerUpdate = Math.max(1, _secondsPerUpdate - 1);
	}
	
	public Seconds getTotalSimulationTime() {
		return totalSimulationTime;
	}
}

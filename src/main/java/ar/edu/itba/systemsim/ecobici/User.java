package ar.edu.itba.systemsim.ecobici;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class User {

	private UserTravelStrategy _travelStrategy;
	private BikeArea bikeArea;
	private BikeStation origin;

	public User(UserTravelStrategy travelStrategy, BikeArea bikeArea, BikeStation origin) {
		_travelStrategy = travelStrategy;
		this.bikeArea = bikeArea;
		this.origin = origin;
	}

	public void recieve(Bike bike) {
		BikeStation destination = _travelStrategy.getDestination(origin);
		long duration = _travelStrategy.getTravelTime(origin, destination);
		new BikeUser(bikeArea, bike, origin).start(destination, duration); 
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", "(no_name)").build();
	}
}

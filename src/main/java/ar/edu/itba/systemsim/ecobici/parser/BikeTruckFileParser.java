package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeStations;
import ar.edu.itba.systemsim.ecobici.BikeTransporterPath;
import ar.edu.itba.systemsim.ecobici.BikeTruck;
import ar.edu.itba.systemsim.util.CollectionUtil;

import com.google.common.collect.Lists;

public class BikeTruckFileParser {

	private List<BikeTruck> _transporters;
	
	public Collection<BikeTruck> getTrucks() {
		return _transporters;
	}
	
	public void load(ConfigurationFiles configurations, Collection<BikeStation> stations) throws IOException {		
		Map<Integer, BikeStation> stationsById = CollectionUtil.asMap(stations, BikeStations.getId());
		_transporters = Lists.newLinkedList();
		for (File file : configurations.getBikeTrucks()) {
			FileInputStream stream = new FileInputStream(file);
			Properties prop = new Properties();
			prop.load(stream);
			stream.close();
			if ("1".equals(prop.getProperty("enabled"))) {
				int id = asInt(prop.getProperty("id"));
				BikeStation origin = stationsById.get(asInt(prop.getProperty("origin")));
				List<BikeStation> bikeStationList = Lists.newLinkedList();
				for (String idString : prop.getProperty("path").split(" ")) {
					bikeStationList.add(stationsById.get(asInt(idString)));
				}
				int maxCapacity = asInt(prop.getProperty("max.bikes"));
				float speed = Float.valueOf(prop.getProperty("mean.speed"));
				BikeTruck transporter = new BikeTruck(id, origin, new BikeTransporterPath(bikeStationList), maxCapacity, speed);
				_transporters.add(transporter);
			}
		}
	}

	private int asInt(String s) {
		return Integer.valueOf(s);
	}
	
}

package ar.edu.itba.systemsim.ecobici;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.newdawn.slick.geom.Vector2f;

import ar.edu.itba.systemsim.core.Consumer;
import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.core.sink.ConsumeEvent;
import ar.edu.itba.systemsim.util.CollectionUtil;

import com.google.common.collect.Iterables;

public class BikeStation extends Consumer<User> {

	private static final EventDispatcher dispatcher = EventDispatcher.instance();
	private static final SimulationTime time = SimulationTime.instance();
	private static final Logger logger = Logger.getLogger(BikeStation.class);

	private int _id;
	private String _name;
	private List<Bike> _bikes = new LinkedList<Bike>();
	private Vector2f _location;
	
	private int minBikesMorning;
	private int maxBikesMorning;
	private int minBikesEvening;
	private int maxBikesEvening;
	
	private int receivedBikes = 0;
	
	public BikeStation(int id, String name, Vector2f location, List<Bike> bikes) {
		super(new BikeStationUserQueue());
		this._id = id;
		this._name = name;
		this._location = location;
		returnAll(bikes);
	}
	
	public void setMorningBikeRange(int min, int max) {
		this.minBikesMorning = min;
		if (max < min) {
			throw new IllegalStateException("Illegal morning bike range for station " + getId());
		}
		this.maxBikesMorning = max;
	}
	
	public void setEveningBikeRange(int min, int max) {
		this.minBikesEvening = min;
		if (max < min) {
			throw new IllegalStateException("Illegal evening bike range for station " + getId());
		}
		this.maxBikesEvening = max;
	}
	
	@Override
	protected boolean isBusy() {
		return super.isBusy() || getBikeCount() == 0;
	}

	@Override
	public void consume(User user) {
		Bike bike = retrieveOne();
		logger.info("User " + getName() + " received bike " + bike.getId());
		user.recieve(bike);
	}
	
	public int getId() {
		return _id;
	}

	public String getName() {
		return _name;
	}

	public Vector2f getLocation() {
		return _location;
	}

	public void returnOne(Bike bike) {
		returnAll(Collections.singletonList(bike));
	}

	public void returnAll(Collection<Bike> bikes) {
		logger.info("Returned " + bikes.size() + " bikes to " + getName());
		for (Bike bike : bikes) {
			bike.returnedToStation();
		}
		_bikes.addAll(bikes);
		receivedBikes += bikes.size();
		dispatcher.dispatchNow(new ConsumeEvent(this, this));
	}


	public Collection<Bike> retrieveBikes(int amount) {
		Collection<Bike> toRetrieve = CollectionUtil.removeFirstN(_bikes, amount);
		for (Bike bike : toRetrieve) {
			bike.retrievedFromStation();
		}
		return toRetrieve;
	}

	public Bike retrieveOne() {
		Collection<Bike> toRetrieve = retrieveBikes(1); 
		return toRetrieve.isEmpty() ? null : Iterables.get(toRetrieve, 0);
	}

	public int getBikeCount() {
		return _bikes.size();
	}

	public Collection<Bike> getBikes() {
		return Collections.unmodifiableCollection(_bikes);
	}
	
	public long getRetreivedCount() {
		return getStats().getConsumed();
	}

	public int getReceivedCount() {
		return receivedBikes;
	}

	public float distanceTo(BikeStation to) {
		return getLocation().distance(to.getLocation());
	}

	public boolean hasBikeShortage() {
		return getBikeShortageCount() != 0;
	}

	public int getBikeShortageCount() {
		int minBikes = time.isMorning() ? minBikesMorning : minBikesEvening;
		int bikeCount = getBikeCount();
		return (bikeCount < minBikes) ? minBikes - bikeCount : 0;	
	}
	
	public boolean hasExtraBikes() {
		return getExtraBikeCount() != 0;
	}

	public int getExtraBikeCount() {
		int maxBikes = time.isMorning() ? maxBikesMorning : maxBikesEvening; 
		int bikeCount = getBikeCount();
		return (bikeCount > maxBikes) ? bikeCount - maxBikes : 0;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
			.append("id", getId())
			.append("name", getName())
			.append("bikes", getBikeCount()).build();
	}

}

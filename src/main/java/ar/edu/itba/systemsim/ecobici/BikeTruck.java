package ar.edu.itba.systemsim.ecobici;

import junit.framework.Assert;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.core.event.EventListener;

public class BikeTruck extends BikeTransporter {

	private static final Logger logger = Logger.getLogger(BikeTruck.class);
	private static final EventDispatcher dispacther = EventDispatcher.instance();
	
	private float _meanSpeed;
	private int _id;
	private BikeTransporterPath _path;
	private int _maxCapacity;

	public BikeTruck(int id, BikeStation origin, BikeTransporterPath path, int maxCapacity, float meanSpeed) {
		Assert.assertNotNull(path);
		_id = id;
		_path = path;
		_maxCapacity = maxCapacity;
		_meanSpeed = meanSpeed;
		setOrigin(origin);
	}

	public void start() {
		arrived();
	}

	public int getId() {
		return _id;
	}
	
	public BikeTransporterPath getPath() {
		return _path;
	}
	
	@Override
	protected void arrived() {
		logger.info("Truck arrived at " + getOrigin().getName());
		loadOrDispatchBikesInCurrentStation();
		setUpNextDestination();
	}
	
	private void loadOrDispatchBikesInCurrentStation() {
		BikeStation station = getOrigin();
		if (station.hasBikeShortage()) {
			int stationBikeShortage = station.getBikeShortageCount(); 
			if (getBikeCount() > 0) {
				int giveOut = Math.min(stationBikeShortage, getBikeCount());
				logger.info("\t T = " + _id + " > Station needs " + stationBikeShortage + " bikes. Giving " + giveOut);
				station.returnAll(retrieveBikes(giveOut));
			}
		} else if (station.hasExtraBikes()) {
			int stationBikeExtraBikes = station.getExtraBikeCount(); 
			int retrieveFromStation = Math.min(stationBikeExtraBikes, _maxCapacity - getBikeCount());
			if (retrieveFromStation > 0) {
				logger.info("\t T = " + _id + " > Station has " + stationBikeExtraBikes + " extra bikes. Taken " + retrieveFromStation);
				addBikes(station.retrieveBikes(retrieveFromStation));
			}
		}
	}
	
	private void setUpNextDestination() {
		BikeStation destination;
		if (getBikeCount() == 0) {
			destination = _path.nextForFetchBikes(getOrigin());
		} else {
			destination = _path.nextForDropBikes(getOrigin());
		}
		if (destination == null) {
			dispacther.dispatch(new ScheduleTravel(this), 5); // TODO: cuanto tiempo de espera?
		} else {
			setUpTravel(destination, _path.travelTime(getOrigin(), destination, _meanSpeed));
		}
	}
	
	@Override
	public void onEvent(Event event) {
		super.onEvent(event);
		if (event instanceof ScheduleTravel) {
			setUpNextDestination();
		}
	}

	private static final class ScheduleTravel extends Event {
		public ScheduleTravel(EventListener sender) {
			super(sender, sender);
		}
	}
}

package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;

public class ConfigurationFiles {

	private File _directory;

	public ConfigurationFiles(File directory) {
		_directory = directory;
	}

	public String getAbsPath() {
		return _directory.getAbsolutePath() + "/";
	}
	
	public File getGlobalConfiguration() {
		return new File(_directory.getAbsolutePath() + "/global.properties");
	}
	
	public File[] getBikeStations() {
		return new File(_directory.getAbsolutePath() + "/station/").listFiles();
	}

	public File[] getBikeUserDestinations() {
		return new File(_directory.getAbsolutePath() + "/bikeuser/destination/").listFiles();
	}

	public File[] getBikeUserUseTimes() {
		return new File(_directory.getAbsolutePath() + "/bikeuser/use_times/").listFiles();
	}

	public File[] getBikeTrucks() {
		return new File(_directory.getAbsolutePath() + "/biketruck/").listFiles();
	}
	
	public File[] getUserArrivalRate() {
		return new File(_directory.getAbsolutePath() + "/userArrivalRate/").listFiles();
	}
}

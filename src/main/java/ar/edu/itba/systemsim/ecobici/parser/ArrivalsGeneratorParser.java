package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.ecobici.ArrivalsGenerator;
import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeStations;
import ar.edu.itba.systemsim.util.CollectionUtil;

public class ArrivalsGeneratorParser {

	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(ArrivalsGeneratorParser.class);
	
	public Map<BikeStation, ArrivalsGenerator> load(ConfigurationFiles configurations, Collection<BikeStation> stationList) throws IOException {
		Map<Integer, BikeStation> stations = CollectionUtil.asMap(stationList, BikeStations.getId());
		Map<BikeStation, ArrivalsGenerator> arrivalsGeneratorsMap = new HashMap<BikeStation, ArrivalsGenerator>();
		
		for (File file : configurations.getUserArrivalRate()) {
			ArrivalsGenerator arrivalsGenerators = new ArrivalsGenerator();
			Properties properties = new Properties();
			properties.load(new FileInputStream(file));
			String stationId = file.getName().replace(".txt", "");
			BikeStation station = stations.get(Integer.parseInt(stationId));
			for (Entry<Object, Object> entries : properties.entrySet()) {
				String[] timeInterval = entries.getKey().toString().trim().split("-");
				int startHour = Integer.valueOf(timeInterval[0]);
				int endHour = Integer.valueOf(timeInterval[1]);
				float mean = Math.min(Float.valueOf(entries.getValue().toString()),1000);
				arrivalsGenerators.addArrivalRate(mean, startHour, endHour);
			}
			arrivalsGeneratorsMap.put(station, arrivalsGenerators);
		}
		return arrivalsGeneratorsMap;
	}

}

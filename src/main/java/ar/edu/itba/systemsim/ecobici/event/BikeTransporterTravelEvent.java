package ar.edu.itba.systemsim.ecobici.event;

import ar.edu.itba.systemsim.core.Updatable;
import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.ecobici.BikeTransporter;

public class BikeTransporterTravelEvent extends Event implements Updatable {

	private BikeTransporter _transporter;
	
	public BikeTransporterTravelEvent(BikeTransporter sender) {
		super(sender, sender);
		this._transporter = sender;
	}

	@Override
	public void update() {
		_transporter.decrementDuration();
	}

}

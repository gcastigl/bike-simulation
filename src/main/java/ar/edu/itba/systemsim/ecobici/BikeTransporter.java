package ar.edu.itba.systemsim.ecobici;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.core.event.EventListener;
import ar.edu.itba.systemsim.ecobici.event.BikeTransporterTravelEvent;
import ar.edu.itba.systemsim.util.CollectionUtil;

public abstract class BikeTransporter implements EventListener {

	private static final EventDispatcher dispatcher = EventDispatcher.instance();
	private static final Logger logger = Logger.getLogger(BikeTransporter.class);
	
	private BikeStation _origin, _desination;
	private long _totalDuration, _duration;
	private List<Bike> _bikes = new LinkedList<>();
	private int travelCount;

	protected List<Bike> getBikes() {
		return _bikes;
	}

	protected List<Bike> retrieveBikes(int amount) {
		return CollectionUtil.removeFirstN(getBikes(), amount);
	}

	protected void addBikes(Collection<Bike> bikes) {
		Assert.assertNotNull(bikes);
		_bikes.addAll(bikes);
	}

	public int getBikeCount() {
		return getBikes().size();
	}

	protected void setUpTravel(BikeStation destination, long duration) {
		_desination = destination;
		_duration = duration;
		_totalDuration = duration;
		logger.info(getClass().getSimpleName() + " traveling [" + _origin.getName() + " => " + _desination.getName() + "] - Time: " + duration);
		dispatcher.dispatch(new BikeTransporterTravelEvent(this), duration);
	}

	@Override
	public void onEvent(Event event) {
		if (event instanceof BikeTransporterTravelEvent) {
			_origin = _desination;
			travelCount++;
			arrived();
		}
	}

	public int getTravelCount() {
		return travelCount;
	}
	
	protected abstract void arrived();
	
	public long getDuration() {
		return _duration;
	}
	
	public void decrementDuration() {
		_duration--;
	}
	
	public BikeStation getOrigin() {
		return _origin;
	}
	
	protected void setOrigin(BikeStation origin) {
		Assert.assertNotNull(origin);
		_origin = origin;
	}
	
	public BikeStation getDestination() {
		return _desination;
	}
	
	protected void setDesination(BikeStation destination) {
		Assert.assertNotNull(destination);
		_desination = destination;
	}
	
	public float completedPercentage() {
		return 1 - (getDuration() / (float) _totalDuration);
	}

	public boolean hasBikes() {
		return !getBikes().isEmpty();
	}

	public boolean arrivedToDestination() {
		return getDuration() == 0;
	}
	
	public boolean between(BikeStation s1, BikeStation s2) {
		if (getDestination() != null && getDestination().equals(s1) && getOrigin().equals(s2)) {
			return true;
		} else if(getDestination() != null && getDestination().equals(s2) && getOrigin().equals(s1)) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE).toString();
	}
}

package ar.edu.itba.systemsim.ecobici;

import java.util.Comparator;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

public class BikeStations {

	public static final Function<BikeStation, Integer> getId() {
		return new Function<BikeStation, Integer>() {
			@Override
			public Integer apply(BikeStation input) {
				return input.getId();
			}
		};
	}

	public static final Predicate<BikeStation> withBikeShortage() {
		return new Predicate<BikeStation>() {
			public boolean apply(BikeStation input) {
				return input.hasBikeShortage();
			}
		};
	}

	public static final Predicate<BikeStation> withExtraBikes() {
		return new Predicate<BikeStation>() {
			public boolean apply(BikeStation input) {
				return input.hasExtraBikes();
			}
		};
	}

	public static final Comparator<BikeStation> sortById() {
		return new Comparator<BikeStation>() {
			@Override
			public int compare(BikeStation o1, BikeStation o2) {
				return Integer.compare(o1.getId(), o2.getId());
			}
		};
	}

}

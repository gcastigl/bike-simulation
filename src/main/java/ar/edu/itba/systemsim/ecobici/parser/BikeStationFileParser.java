package ar.edu.itba.systemsim.ecobici.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.newdawn.slick.geom.Vector2f;

import ar.edu.itba.systemsim.ecobici.Bike;
import ar.edu.itba.systemsim.ecobici.BikeStation;

import com.google.common.collect.Lists;

public class BikeStationFileParser {

	private int lastBikeId = 0;
	private Collection<BikeStation> _stations;

	public Collection<BikeStation> getStations() {
		return _stations;
	}
	
	public void load(ConfigurationFiles configurations) throws IOException {
		Properties properties = new Properties();
		_stations = Lists.newLinkedList();
		lastBikeId = 0;
		for (File file : configurations.getBikeStations()) {
			properties.clear();
			FileInputStream inputStream = new FileInputStream(file);
			properties.load(inputStream);
			inputStream.close();
			_stations.add(build(properties));
		}
	}
	
	private BikeStation build(Properties properties) {
		int id = asInt(properties.getProperty("id"));
		String name = properties.getProperty("name");
		String[] locationString = properties.getProperty("location").split(" ");
		Vector2f location = new Vector2f(Float.valueOf(locationString[0]), Float.valueOf(locationString[1]));
		int bikeCount = asInt(properties.getProperty("bikes"));
		BikeStation station = new BikeStation(id, name, location, buildBikes(bikeCount, lastBikeId));
		parseDistributions(properties, station, "morning");
		parseDistributions(properties, station, "evening");
		return station;
	}
	
	private List<Bike> buildBikes(int amount, int initialId) {
		List<Bike> bikes = new LinkedList<Bike>();
		for (int i = 0; i < amount; i++) {
			bikes.add(new Bike(initialId + i));
		}
		lastBikeId += amount;
		return bikes;
	}
	
	private void parseDistributions(Properties properties, BikeStation station, String prefix) {
		String[] bikeValues = properties.getProperty(prefix + ".bikes").split(" ");
		int minBikes = asInt(bikeValues[0]);
		int maxBikes = asInt(bikeValues[1]);
		if (prefix.equals("morning")) {
			station.setMorningBikeRange(minBikes, maxBikes);
		} else if (prefix.equals("evening")) {
			station.setEveningBikeRange(minBikes, maxBikes);
		} else {
			throw new IllegalStateException(prefix + " is not valid");
		}
	}

	private int asInt(String s) {
		return Integer.valueOf(s);
	}
	
	private float asFloat(String s) {
		return Float.valueOf(s);
	}
	
}

package ar.edu.itba.systemsim.ecobici;

import ar.edu.itba.systemsim.core.rand.RandomGenerator;

public class BikeStationUserArrival implements RandomGenerator {

	private ArrivalsGenerator _arrivalsRateGenerators;
	
	public BikeStationUserArrival(ArrivalsGenerator arrivalsGenerators) {
		_arrivalsRateGenerators = arrivalsGenerators;
	}

	@Override
	public float generate() {
		return _arrivalsRateGenerators.getArrivalRate();
	}
	
	@Override
	public String toString() {
		return _arrivalsRateGenerators.toString();
	}
	
}

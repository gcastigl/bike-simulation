package ar.edu.itba.systemsim.ecobici;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;

public class BikeUser extends BikeTransporter {
	
	private static final Logger logger = Logger.getLogger(BikeUser.class);

	private BikeArea _bikeArea;
	
	public BikeUser(BikeArea bikeArea, Bike bike, BikeStation origin) {
		super();
		_bikeArea = bikeArea;
		getBikes().add(bike);
		setOrigin(origin);
	}

	public void start(BikeStation destination, long duration) {
		_bikeArea.add(this);
		setUpTravel(destination, duration);
	}
	
	@Override
	public void arrived() {
		logger.info("User ended trip at " + getDestination().getName());
		_bikeArea.remove(this);
		getDestination().returnOne(removeBike());
	}

	private Bike removeBike() {
		return getBikes().remove(0);
	}

	public Bike getBike() {
		return hasBikes() ? getBikes().get(0) : null;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE).toString();
	}
}

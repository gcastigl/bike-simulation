package ar.edu.itba.systemsim.ecobici.parser;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.ecobici.ArrivalsGenerator;
import ar.edu.itba.systemsim.ecobici.BikeArea;
import ar.edu.itba.systemsim.ecobici.BikeStation;
import ar.edu.itba.systemsim.ecobici.BikeStationUserArrival;
import ar.edu.itba.systemsim.ecobici.StationDestinationMatrix;
import ar.edu.itba.systemsim.ecobici.UserProducer;
import ar.edu.itba.systemsim.ecobici.UserTimeTravel;
import ar.edu.itba.systemsim.ecobici.UserTravelStrategy;

public class BikeAreaFileParser {

	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(BikeAreaFileParser.class);

	private BikeStationFileParser stationParser = new BikeStationFileParser();
	private BikeTruckFileParser truckFileParser = new BikeTruckFileParser();

	public BikeArea build(ConfigurationFiles configurations) throws IOException {
		BikeArea bikeArea = new BikeArea();
		stationParser.load(configurations);
		bikeArea.addBikeStations(stationParser.getStations());
		buildProducers(configurations, bikeArea);
		truckFileParser.load(configurations, bikeArea.getBikeStations());
		bikeArea.addBikeTrucks(truckFileParser.getTrucks());
		return bikeArea;
	}
	
	private void buildProducers(ConfigurationFiles configurations, BikeArea bikeArea) throws IOException {
		StationDestinationMatrixFileParser destinationMatrixParser = new StationDestinationMatrixFileParser();
		List<StationDestinationMatrix> destinationMatrixList = destinationMatrixParser.load(configurations, bikeArea.getBikeStations());
		UserTimeTravel userTimeTravel = new UserTimeTravelParser().load(configurations, bikeArea.getBikeStations());
		Map<BikeStation, ArrivalsGenerator> arrivalGeneratorsMap = new ArrivalsGeneratorParser().load(configurations, bikeArea.getBikeStations());
		UserTravelStrategy travelStrategy = new UserTravelStrategy(destinationMatrixList, userTimeTravel);
		ArrivalsGenerator arrivalsGenerator;
		for (BikeStation station : bikeArea.getBikeStations()) {
			arrivalsGenerator = arrivalGeneratorsMap.get(station);
			BikeStationUserArrival userArrival = new BikeStationUserArrival(arrivalsGenerator);
			bikeArea.addProducer(new UserProducer(travelStrategy, bikeArea, station, userArrival));
		}
	}

}

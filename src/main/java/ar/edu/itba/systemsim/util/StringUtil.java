package ar.edu.itba.systemsim.util;

public class StringUtil {

	public static String trim(String s, int len) {
		return s.length() < len ? s : s.substring(0, len);
	}
}

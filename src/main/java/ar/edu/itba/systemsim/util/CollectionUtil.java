package ar.edu.itba.systemsim.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class CollectionUtil {

	public static <T> List<T> removeFirstN(List<T> list, int amount) {
		List<T> removed = Lists.newLinkedList(list.subList(0, amount));
		list.removeAll(removed);
		return removed;
	}

	public static <T> Map<Integer, T> asMap(Collection<T> collection, Function<T, Integer> key) {
		Map<Integer, T> map = Maps.newHashMap();
		for (T elem : collection) {
			map.put(key.apply(elem), elem);
		}
		return map;
	}
	
	public static <T> boolean hasNulls(Collection<T> collection) {
		Predicate<T> notNulls = Predicates.notNull();
		return !Collections2.filter(collection, Predicates.not(notNulls)).isEmpty();
	}
}

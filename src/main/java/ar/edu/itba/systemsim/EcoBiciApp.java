package ar.edu.itba.systemsim;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.ecobici.BikeArea;
import ar.edu.itba.systemsim.ecobici.SimulationRunsStats;
import ar.edu.itba.systemsim.ecobici.SimulationTime;
import ar.edu.itba.systemsim.ecobici.parser.BikeAreaFileParser;
import ar.edu.itba.systemsim.ecobici.parser.ConfigurationFiles;
import ar.edu.itba.systemsim.gui.BikeAreaRenderer;
import ar.edu.itba.systemsim.gui.KeyboardHandler;
import ar.edu.itba.systemsim.gui.KeyboardListener;
import ar.edu.itba.systemsim.gui.StatsRenderer;

public class EcoBiciApp extends BasicGame {

	public static void main(String[] args) {
		try {
			AppGameContainer app = new AppGameContainer(new EcoBiciApp(args[0]));
			app.setVSync(true);
			app.setDisplayMode(1300, 700, false);
			app.setUpdateOnlyWhenVisible(false);
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
	private final SimulationTime _time = SimulationTime.instance();
	private final EventDispatcher _dispatcher = EventDispatcher.instance();
	private final Properties globalProperties = new Properties();
	private final ConfigurationFiles _configurations;

	private BikeArea _bikeArea;
	private BikeAreaRenderer renderer;
	private KeyboardHandler _keyboardHandler;
	private StatsRenderer statsRenderer;
	private long totalElapsed = 0;
	private int _totalRuns;
	private int _minTimeStep;

    public EcoBiciApp(String configurationDir) {
		super("Simulacion Eco-bicis");
		_configurations = new ConfigurationFiles(new File(configurationDir));
		try {
			globalProperties.getProperty("globalProperties");
			globalProperties.load(new FileInputStream(_configurations.getGlobalConfiguration()));
			_time.setSecondsPerUpdate(Integer.valueOf(globalProperties.getProperty("secondsPerUpdate")));
			_totalRuns = Integer.valueOf(globalProperties.getProperty("totalRuns"));
			_minTimeStep = Integer.valueOf(globalProperties.getProperty("minTimeStep"));
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

    @Override
    public void init(GameContainer gc) throws SlickException {
    	try {
    		_time.setTime(8, 0, 0);
    		_dispatcher.clear();
    		System.gc();
	    	_bikeArea = new BikeAreaFileParser().build(_configurations);
	    	renderer = new BikeAreaRenderer(_configurations.getAbsPath() + globalProperties.getProperty("backgroundImg"));
	    	statsRenderer = new StatsRenderer();
	    	_keyboardHandler = new KeyboardHandler(_time);
	    	gc.getInput().addKeyListener(new KeyboardListener(renderer));
	    	_bikeArea.initialize();
    	} catch (Exception e) {
    		throw new IllegalStateException(e);
    	}
    }

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		renderer.render(g, _bikeArea, gc);
		statsRenderer.render(g,_bikeArea, gc);
	}

	@Override
	public void update(GameContainer gc, int elapsedTime) throws SlickException {
		_keyboardHandler.update(gc, elapsedTime);
		int currentRuns = SimulationRunsStats.instance().getRunsCount();
		if (currentRuns < _totalRuns) {
			totalElapsed += elapsedTime;
			if (totalElapsed >= _minTimeStep) {
				_time.update();
				if (!_bikeArea.isEndOfDay()) {
					for (int times = 0; times < _time.getUpdateTime(); times++) {
						_dispatcher.update();
					}
				} else {
					SimulationRunsStats.instance().simulationFinished(_bikeArea);
					gc.reinit();
				}
				totalElapsed = 0;
			}
		} else if (currentRuns == _totalRuns) {
			SimulationRunsStats.instance().getResults();
			gc.exit();
		}
	}
}

package ar.edu.itba.systemsim.core;

import ar.edu.itba.systemsim.core.sink.Sink;
import ar.edu.itba.systemsim.core.stats.ConsumerStats;

import com.google.common.base.Function;

public class Consumers {

	public static final <T> Function<Consumer<T>, ConsumerStats<T>> getStats() {
		return new Function<Consumer<T>, ConsumerStats<T>>() {
			public ConsumerStats<T> apply(Consumer<T> input) {
				return input.getStats();
			}
		};
	}

	public static final <T> Function<Consumer<T>, Sink<T>> getSink() {
		return new Function<Consumer<T>, Sink<T>>() {
			public Sink<T> apply(Consumer<T> input) {
				return input.getSink();
			}
		};
	}
}

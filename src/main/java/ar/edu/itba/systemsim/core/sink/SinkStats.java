package ar.edu.itba.systemsim.core.sink;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SinkStats<T> {

    public int _enqueued, _consumed, _leftOut;

    public void onEnqueued(T elem) {
        _enqueued++;
    }

    public void onConsumed(T elem) {
        _consumed++;
    }

    public void onLeftOut(T elem) {
        _leftOut++;
    }

    public int getLeftOut() {
    	return _leftOut;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("Enqueued", _enqueued)
            .append("Consumed", _consumed)
            .append("Left Out", _leftOut)
            .build();
    }
}

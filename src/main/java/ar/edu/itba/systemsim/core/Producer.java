package ar.edu.itba.systemsim.core;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.core.event.EventListener;
import ar.edu.itba.systemsim.core.rand.RandomGenerator;
import ar.edu.itba.systemsim.core.sink.Sink;
import ar.edu.itba.systemsim.core.stats.ProducerStats;

public abstract class Producer<T> implements EventListener {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(Producer.class);
	
	private static final EventDispatcher dispatcher = EventDispatcher.instance();

    private Sink<T> _sink;
    private ProducerStats<T> _stats;
    private RandomGenerator _generator;
    private RandomGenerator _timeOutGenerator;

    public Producer(Sink<T> sink, RandomGenerator generator) {
        _sink = sink;
        _stats = new ProducerStats<T>();
        _generator = generator;
    }
    
    public Producer(Sink<T> sink, RandomGenerator generator, RandomGenerator timeOutGenerator) {
    	this(sink, generator);
        _timeOutGenerator = timeOutGenerator;
    }

    public ProducerStats<T> getStats() {
        return _stats;
    }

    public Sink<T> getSink() {
		return _sink;
	}

    public void start() {
    	long time = (long) _generator.generate();
    	dispatcher.dispatch(new ProduceEvent(this), time);
    }

    @Override
    public void onEvent(Event event) {
    	if (event instanceof ProduceEvent) {
			T elem = produce();
			if (_timeOutGenerator != null) {
				_sink.enqueue(elem, (long)_timeOutGenerator.generate());
			} else {
				_sink.enqueue(elem);
			}
			_stats.onProduced(elem);
			start();
    	}
    }
    
    protected abstract T produce();

    private static class ProduceEvent extends Event {

		public ProduceEvent(EventListener sender) {
			super(sender, sender);
		}
    	
    }

}

package ar.edu.itba.systemsim.core.rand;

import java.util.Random;

public class GaussianRandomGenerator implements RandomGenerator {

	private Random random;
	private float _mean, _stDev;

	public GaussianRandomGenerator(float mean, float stDev) {
		random = new Random();
		setMean(mean);
		setStDev(stDev);
	}

	public void setMean(float mean) {
		_mean = mean;
	}

	public void setStDev(float stDev) {
		_stDev = stDev;
	}

	@Override
	public float generate() {
		return (float) (_stDev * random.nextGaussian() + _mean);
	}
}

package ar.edu.itba.systemsim.core.rand;

public class CappedGaussianRandomGenerator extends GaussianRandomGenerator {

	private float _min, _max;

	public CappedGaussianRandomGenerator(float mean, float stDev, float stdDevTimes, boolean alwaysPositive) {
		this(mean, stDev, mean - stdDevTimes * stDev, mean + stdDevTimes * stDev);
		if (alwaysPositive) {
			_min = Math.max(0, _min);
		}
	}

	public CappedGaussianRandomGenerator(float mean, float stDev, float min, float max) {
		super(mean, stDev);
		setLimits(min, max);
	}

	public void setLimits(float min, float max) {
		_min = min;
		_max = max;
	}

	@Override
	public float generate() {
		return Math.max(_min, Math.min(_max, super.generate()));
	}

}

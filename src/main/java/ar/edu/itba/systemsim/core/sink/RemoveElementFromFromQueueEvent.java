package ar.edu.itba.systemsim.core.sink;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventListener;

public class RemoveElementFromFromQueueEvent<T> extends Event {

	private T _element;

	public RemoveElementFromFromQueueEvent(EventListener sender, T element) {
		super(sender, sender);
		_element = element;
	}

	public T getElement() {
		return _element;
	}
}

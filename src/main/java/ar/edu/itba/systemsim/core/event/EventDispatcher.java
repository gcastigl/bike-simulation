package ar.edu.itba.systemsim.core.event;

import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.Updatable;

import com.google.common.collect.Lists;

public class EventDispatcher implements Updatable {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(EventDispatcher.class);
	private static final EventDispatcher _instance = new EventDispatcher();

	public static EventDispatcher instance() {
		return _instance;
	}

	private PriorityQueue<PendingEvent> pendingEvents = new PriorityQueue<>();
	private List<PendingEvent> newEvents = Lists.newLinkedList();
	private long totalEvents;
	
	public void clear() {
		newEvents.clear();
		pendingEvents.clear();
		totalEvents = 0;
	}

	public void dispatchNow(Event event) {
		dispatch(event, 0);
	}

	public void dispatch(Event event, long delay) {
//		logger.info("Enqueued " + event.getClass().getSimpleName() + " | Delay: " + delay);
		totalEvents++;
		if (delay <= 0) {
			event.send();
		} else {
			newEvents.add(new PendingEvent(delay, event));
		}
	}

	@Override
	public void update() {
		pendingEvents.addAll(newEvents);
		newEvents.clear();
		Iterator<PendingEvent> itarator = pendingEvents.iterator();
		while (itarator.hasNext()) {
			PendingEvent pendingEvent = itarator.next();
			pendingEvent._delay--;
			if (pendingEvent._event instanceof Updatable) {
				Updatable event = (Updatable) (pendingEvent._event);
				event.update();
			}
			if (pendingEvent._delay <= 0) {
				itarator.remove();
				pendingEvent._event.send();
			}
		}
	}

	public int queueSize() {
		return pendingEvents.size() + newEvents.size();
	}
	
	public long getTotalEvents() {
		return totalEvents;
	}

	private static class PendingEvent implements Comparable<PendingEvent> {

		Long _delay;
		Event _event;

		public PendingEvent(long time, Event event) {
			_delay = time;
			_event = event;
		}

		@Override
		public int compareTo(PendingEvent o) {
			return _delay.compareTo(o._delay);
		}

	}
}

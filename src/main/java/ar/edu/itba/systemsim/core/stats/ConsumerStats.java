package ar.edu.itba.systemsim.core.stats;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ConsumerStats<T> {

    private long _consumed;
    private long _busyTicks, _idleTicks;

    public void onConsumed(T elem) {
        _consumed++;
    }

    public void onBusyTick() {
        _busyTicks++;
    }

    public void onIdleTick() {
        _idleTicks++;
    }

    public long getConsumed() {
        return _consumed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("Consumed", _consumed)
            .append("Idle", _idleTicks)
            .append("Busy", _busyTicks)
            .build();
    }
}

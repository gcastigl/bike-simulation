package ar.edu.itba.systemsim.core.rand;

public interface RandomGenerator {

    float generate();

}

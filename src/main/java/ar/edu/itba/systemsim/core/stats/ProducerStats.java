package ar.edu.itba.systemsim.core.stats;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ProducerStats<T> {

    private int _produced;

    public void onProduced(T elem) {
        _produced++;
    }

    public int getProduced() {
        return _produced;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("Produced", _produced)
            .build();
    }
}

package ar.edu.itba.systemsim.core.rand;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class ExponentialRandomDistribution implements RandomGenerator {

	private ExponentialDistribution expDistribution;
	
	public ExponentialRandomDistribution(float mean) {
		expDistribution = new ExponentialDistribution(mean);
	}
	
	@Override
	public float generate() {
		return (float) expDistribution.sample();
	}

}

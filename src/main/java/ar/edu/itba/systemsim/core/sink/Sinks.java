package ar.edu.itba.systemsim.core.sink;

import com.google.common.base.Function;

public class Sinks {

	public static final <T> Function<Sink<T>, SinkStats<T>> getStats() {
		return new Function<Sink<T>, SinkStats<T>>() {
			public SinkStats<T> apply(Sink<T> input) {
				return input.getStats();
			}
		};
	}

}

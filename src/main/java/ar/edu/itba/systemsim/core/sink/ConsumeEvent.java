package ar.edu.itba.systemsim.core.sink;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventListener;

public class ConsumeEvent extends Event {

	public ConsumeEvent(EventListener sender, EventListener receiver) {
		super(sender, receiver);
	}

}

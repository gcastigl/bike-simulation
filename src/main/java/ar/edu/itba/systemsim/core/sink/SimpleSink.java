package ar.edu.itba.systemsim.core.sink;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventDispatcher;
import ar.edu.itba.systemsim.core.event.EventListener;

public class SimpleSink<T> implements Sink<T> {

	private static final EventDispatcher dispatcher = EventDispatcher.instance();
	private static final Logger logger = Logger.getLogger(SimpleSink.class);

	private List<T> queue = new LinkedList<>();
    private int _maxSize;
    private SinkStats<T> _stats;
    private List<EventListener> listeners = new LinkedList<>();

    public SimpleSink() {
        this(-1);
    }

    public SimpleSink(int maxSize) {
        _maxSize = maxSize;
        _stats = new SinkStats<T>();
    }

    @Override
    public SinkStats<T> getStats() {
        return _stats;
    }

    @Override
    public void enqueue(T elem) {
    	if (isFull()) {
    		logger.info("Estacion llena");
    		_stats.onLeftOut(elem);
    	} else {
    		_stats.onEnqueued(elem);
    		queue.add(elem);
    		for (EventListener listener : listeners) {
    			dispatcher.dispatchNow(new ConsumeEvent(this, listener));
    		}
    	}
    }

    @Override
    public void enqueue(T elem, long maxWaitingTime) {
    	enqueue(elem);
    	dispatcher.dispatch(new RemoveElementFromFromQueueEvent<T>(this, elem), maxWaitingTime);
    }

    public boolean isFull() {
        return _maxSize >= 0 && size() >= _maxSize;
    }

    public int size() {
        return queue.size();
    }

	@Override
	@SuppressWarnings("unchecked")
    public void onEvent(Event event) {
    	if (event instanceof RemoveElementFromFromQueueEvent) {
    		T element = ((RemoveElementFromFromQueueEvent<T>) event).getElement();
    		if (queue.remove(element)) {
    			logger.info(element + " has left the station");
    			_stats.onLeftOut(element);
    		}
    	}
    }

    @Override
    public void suscribe(EventListener listener) {
    	listeners.add(listener);
    }
    
    @Override
    public void unsuscribe(EventListener listener) {
    	listeners.remove(listener);
    }
    
    @Override
    public T removeOne() {
        if (queue.isEmpty()) {
            return null;
        }
        T elem = queue.remove(0);
        _stats.onConsumed(elem);
        return elem;
    }

    @Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE).toString();
	}
}

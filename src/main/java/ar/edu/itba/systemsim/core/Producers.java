package ar.edu.itba.systemsim.core;

import ar.edu.itba.systemsim.core.stats.ProducerStats;

import com.google.common.base.Function;

public class Producers {

	public static final <T> Function<Producer<T>, ProducerStats<T>> getStats() {
		return new Function<Producer<T>, ProducerStats<T>>() {
			public ProducerStats<T> apply(Producer<T> input) {
				return input.getStats();
			}
		};
	}
	
}

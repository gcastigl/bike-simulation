package ar.edu.itba.systemsim.core;

public interface Updatable {

    void update();

}

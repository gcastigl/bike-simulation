package ar.edu.itba.systemsim.core;

import ar.edu.itba.systemsim.core.event.Event;
import ar.edu.itba.systemsim.core.event.EventListener;
import ar.edu.itba.systemsim.core.sink.ConsumeEvent;
import ar.edu.itba.systemsim.core.sink.SimpleSink;
import ar.edu.itba.systemsim.core.stats.ConsumerStats;

public abstract class Consumer<T> implements EventListener {

    private SimpleSink<T> _sink;
    private int _busyTicks;
    private ConsumerStats<T> _stats;

    public Consumer(SimpleSink<T> sink) {
        this(sink, new ConsumerStats<T>());
    }

    public Consumer(SimpleSink<T> sink, ConsumerStats<T> stats) {
        _sink = sink;
        _sink.suscribe(this);
        _busyTicks = 0;
        _stats = stats;
    }

    public ConsumerStats<T> getStats() {
        return _stats;
    }

    public SimpleSink<T> getSink() {
		return _sink;
	}
    
    public int getSinkSize() {
    	return _sink.size();
    }

    @Override
    public void onEvent(Event event) {
    	if (event instanceof ConsumeEvent) {
    		if (isBusy()) {
    			// TODO: arreglar tema de los ticks
    			_stats.onBusyTick();
    			_busyTicks--;
    		} else {
    			T elem = _sink.removeOne();
    			if (elem != null) {
    				_stats.onConsumed(elem);
    				consume(elem);
    			} else {
    				_stats.onIdleTick();
    			}
    		}
    	}
    }
    
    protected boolean isBusy() {
        return _busyTicks > 0;
    }

    protected void delay(int ticks) {
         _busyTicks = ticks;
    }

    protected abstract void consume(T elem);

}

package ar.edu.itba.systemsim.core;

import java.util.Collection;

import com.google.common.base.Function;

public class Math {

	public static final Function<Collection<Float>, Float> average() {
		return new Function<Collection<Float>, Float>() {
			@Override
			public Float apply(Collection<Float> input) {
				float total = 0;
				for (float num : input) {
					total += num;
				}
				return total / input.size();
			}
			
		};
	}
	
	public static final Function<Collection<Float>, double[]> toDoubleArray() {
		return new Function<Collection<Float>, double[]>() {
			@Override
			public double[] apply(Collection<Float> input) {
				double[] values = new double[input.size()];
				int index = 0;
				for (float num : input) {
					values[index++] = num;
				}
				return values;
			}
			
		};
	}
}

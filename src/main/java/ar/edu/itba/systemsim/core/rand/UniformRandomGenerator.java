package ar.edu.itba.systemsim.core.rand;

public class UniformRandomGenerator implements RandomGenerator {

    private int _min, _max;

    public UniformRandomGenerator(int min, int max) {
        setInterval(min, max);
    }

    @Override
    public float generate() {
        return (float) (Math.random() * (_max - _min) + _min);
    }
    
    public void setInterval(int min, int max) {
    	_min = min;
        _max = max;
    }
}

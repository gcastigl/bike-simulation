package ar.edu.itba.systemsim.core.sink;

import ar.edu.itba.systemsim.core.event.EventListener;

public interface Sink<T> extends EventListener {

	void enqueue(T elem);

	void enqueue(T elem, long maxWaitingTime);

	T removeOne();

	void suscribe(EventListener listener);
	
	void unsuscribe(EventListener listener);

	SinkStats<T> getStats();

}

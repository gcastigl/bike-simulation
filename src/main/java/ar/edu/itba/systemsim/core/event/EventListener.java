package ar.edu.itba.systemsim.core.event;


public interface EventListener {

	void onEvent(Event event);

}
